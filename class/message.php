<?
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.

/**
 * class to handle messages to later be shown to users
 */

class Message {
  private $m_message;
  private $m_errorcode;
  /**
   * @param message to be shown to user
   * @param errorcode 0 means no error, 1 means undefined error.
   */
  const NOERROR = 0;
  const SOMEERROR = 1;
  public function __construct($message, $errorcode) {
    $this->m_message=$message;
    $this->m_errorcode=$errorcode;
  }

  public function message() {
    return $this->m_message;
  }

  public function errorcode() {
    return $this->m_errorcode;
  }
}

?>