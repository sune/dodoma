<?
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.


/**
 *  interface to the database, everything sql'ish goes here and only nice values is accepted and objects returned
 */
class db {
  private $conn;

  public function __construct($host, $username, $passwd, $dbname) {
    $this->conn = new mysqli($host,$username,$passwd,$dbname);
    if($this->conn->connect_errno) {
      die("failed to connect to database: " . $conn->connect_error);
    }
  }

  private function noteHeadersForFolder($folderId) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("select title, id from note where folder=? order by title asc")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("i",$folderId);
    if(! $stmt->execute()) {
      die("sql execute failed " . $stmt->errno );
    }
    $stmt->bind_result($title,$id);
    $noteheaders = array();
    while($stmt->fetch()) {
      $noteheaders[] = new NoteHeader($title,$id);
    }
    return $noteheaders;
  }

  public function folders() {
    $folders=array();
    $folderresult = $this->conn->query("select * from folder order by name asc");
    while($row=$folderresult->fetch_assoc()) {
      $name = $row['name'];
      $id = $row['id'];
      $notes = $this->noteHeadersForFolder($id);
      $folders[]= new Folder($name,$id,$notes);
    }
    return $folders;
  }

  private function folderExists($foldername) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("SELECT name FROM folder WHERE name = ?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("s",$foldername);
    $stmt->execute();
    $stmt->bind_result($foo);
    if($stmt->fetch()) {
      return true;
    }
    return false;
  }

  private function folderIdExists($foldername) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("SELECT name FROM folder WHERE id = ?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("s",$foldername);
    $stmt->execute();
    $stmt->bind_result($foo);
    if($stmt->fetch()) {
      return true;
    }
    return false;
  }

  public function createFolder($foldername) {
    if($this->folderExists($foldername)) {
      return new Message("folder exists",Message::SOMEERROR);
    }
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("INSERT INTO folder ( name ) VALUES (?)")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("s",$foldername);
    if(!$stmt->execute()) {
      die("execute query failed");
    }
    return new Message("folder created",Message::NOERROR);
  }

  function createNote($notename, $folderid) {
    if(!$this->folderIdExists($folderid)) {
      return new Message("folder doesn't exist",Message::SOMEERROR);
    }
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("INSERT INTO note ( title, folder ) VALUES (? , ?)")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("si", $notename, $folderid);
    $stmt->execute();
    return new Message("Note created",Message::NOERROR);
  }

  public function noteById($noteid) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("select title, id, content from note where id=?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("i",$noteid);
    if(! $stmt->execute()) {
      die("sql execute failed " . $stmt->errno );
    }
    $stmt->bind_result($title,$id, $content);
    if($stmt->fetch()) {
      return new Note($title,$id,$content);
    }
    return null;
  }

  public function updateNote($noteid, $data) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("UPDATE note SET content = ? WHERE id = ?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("si", $data, $noteid);
    $stmt->execute();
  }

  public function deleteNote($noteid) {
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("DELETE FROM note WHERE id = ?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("i", $noteid);
    if(!$stmt->execute()) {
      return new Message("An error happened. Note not deleted",Message::SOMEERROR);
    }
    return new Message("Note deleted",Message::NOERROR);
  }

  public function deleteFolder($folderid) {
    $notes = $this->noteHeadersForFolder($folderid);
    if(count($notes)>0) {
      return new Message("Notes in this folder, doing nothing",Message::SOMEERROR);
    }
    $stmt = $this->conn->stmt_init();
    if(!$stmt->prepare("DELETE FROM folder WHERE id = ?")) {
      die("failed to prepare statement: " . $stmt->error );
    }
    $stmt->bind_param("i", $folderid);
    if(!$stmt->execute()) {
      return new Message("An error happened. Folder not deleted",Message::SOMEERROR);
    }
    return new Message("Folder deleted",Message::NOERROR);
  }
}
?>