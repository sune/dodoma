<?php
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.


/**
 * Main application class
 *
 * @package dodoma
 */

include("class/folder.php");
include("class/note.php");
include("class/message.php");

include("class/db.php");

include("config.php");

class Dodoma {
  private $m_current_note = NULL;
  private $m_folders;
  private $m_messages = array();


  /**
   * returns a tree of folders and notes wrapped in nested, unordered lists
   */
  public function createFolderTree() {
    $tree  = '<div id="foldertree">';
    $tree .= '<ul>';
    foreach($this->m_folders as &$folder) {
      $tree .= '<li>';
      $tree .= htmlspecialchars($folder->name());
      $tree .= '<ul>';
      $notes = $folder->notes();
      foreach($notes as $note) {
        $tree .= "<li>";
        $tree .= "<a href =\"".$_SERVER['PHP_SELF']."?note=".$note->id()."\">";
        if(!is_null($this->m_current_note) && $note->id() == $this->m_current_note->id()) {
          $tree .= "<b>";
        }
        $tree .= htmlspecialchars($note->title());
        if(!is_null($this->m_current_note) && $note->id() == $this->m_current_note->id()) {
          $tree .= "</b>";
        }
        $tree .= "</a>";
        $tree .= "</li>";
      }
      $tree .= '</ul>';
      $tree .= '</li>';
    }
    $tree .= '</ul>';
    $tree .= '</div>';
    return $tree;
  }

  /**
   * formats current selected note, or if no note is selected, it returns the given default text
   */
  public function formatCurrentNote($defaulttext) {
    if(is_null($this->m_current_note)) {
      return $defaulttext;
    }
    $formatted  = "";
    $formatted .= "<h2>";
    $formatted .= htmlspecialchars($this->m_current_note->title());
    $formatted .= "</h2>";
    $formatted .= "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
    $formatted .= "<textarea name=\"notetext\" id=\"maintext\">";
    $formatted .= htmlspecialchars($this->m_current_note->content());
    $formatted .= "</textarea>";
    $formatted .= "<input type=\"hidden\" name=\"noteid\" value=\"".$this->m_current_note->id()."\" />";
    $formatted .= "<div class=\"button\"><input type=\"submit\" name=\"update-note\" value=\"Save\" /></div>";
    $formatted .= "</form>";
    return $formatted;
  }

  /**
   * formats any messages given during execution of latest action. Usually it is error messages of some kind
   */

  function formatMessages() {
    $formatted  = "<div id=\"messages\">";
    foreach($this->m_messages as $message) {
      $formatted .= $message->message();
      $formatted .= "</ br>";
    }
    $formatted .= "</div>";
    return $formatted;
  }

  /**
   * returs a toolbox that gives the possibility to create and delete notes and folders
   */
  function createToolBox() {
    $toolbox  = "<div id=\"toolbox\">";

    $noteid = "";
    if(!is_null($this->m_current_note)) {
      $noteid = "<input type=\"hidden\" name=\"noteid\" value=\"".$this->m_current_note->id()."\" />";
    }

    //create note
    $toolbox .= "<div id=\"create-note-toolbox\"  class=\"toolbox\">";
    $toolbox .= "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
    $toolbox .= "<input type=\"text\" name=\"notename\" class=\"input\" />";
    $toolbox .= "<select name=\"folder\" class=\"input\">";
    $toolbox .= "<option value=\"-1\">Select folder ..</option>";
    foreach($this->m_folders as $folder) {
      $toolbox .= "<option value=\"".$folder->id()."\">".htmlspecialchars($folder->name())."</option>";
    }
    $toolbox .= "</select>";
    $toolbox .= "<div class=\"button\"><input type=\"submit\" name=\"create-note\" value=\"New note\"></div>";
    $toolbox .= $noteid;
    $toolbox .= "</form>";
    $toolbox .= "</div>";

    //delete note
    if(!is_null($this->m_current_note)) {
      $toolbox .= "<div id=\"delete-note-toolbox\"  class=\"toolbox\">";
      $toolbox .= "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
      $toolbox .= "<span><input type=\"checkbox\" name=\"iamsure\" /> I am sure</span>";
      $toolbox .= $noteid;
      $toolbox .= "<div class=\"button\"><input type=\"submit\" name=\"delete-note\" class=\"button\" value=\"Delete active note\" /></div>";
      $toolbox .= "</form>";
      $toolbox .= "</div>";
    }

    //create folder
    $toolbox .= "<div id=\"create-folder-toolbox\" class=\"toolbox\">";
    $toolbox .= "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
    $toolbox .= "<input type=\"text\" name=\"foldername\" class=\"input\" />";
    $toolbox .= "<div class=\"button\"><input type=\"submit\" name=\"create-folder\" value=\"New folder\" /></div>";
    $toolbox .= $noteid;
    $toolbox .= "</form>";
    $toolbox .= "</div>";

    //delete folder
    $toolbox .= "<div id=\"delete-folder-toolbox\"  class=\"toolbox\">";
    $toolbox .= "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
    $toolbox .= "<select name=\"folderid\" class=\"input\">";
    $toolbox .= "<option value=\"-1\">Select folder ..</option>";
    foreach($this->m_folders as $folder) {
      if(count($folder->notes())==0) {
        $toolbox .= "<option value=\"".$folder->id()."\">".htmlspecialchars($folder->name())."</option>";
      }
    }
    $toolbox .= "</select>";
    $toolbox .= "<div class=\"button\"><input type=\"submit\" name=\"delete-folder\" value=\"Delete folder\" /></div>";
    $toolbox .= $noteid;
    $toolbox .= "</form>";
    $toolbox .= "</div>";

    $toolbox .= "</div>";
    return $toolbox;
  }


  /**
   * returns the title of current note, or if no current note, the given argument
   */
  public function title($defaulttitle) {
    if(is_null($this->m_current_note)) {
      return $defaulttitle;
    } else {
      return htmlspecialchars($this->m_current_note->title());
    }
  }

  private function redirect($noteid) {
    $_SESSION["redirecting"] = "yes";
    $_SESSION["messages"] = $this->m_messages;
    $append = ($noteid >=0) ? "?note=".$noteid : "";
    $http = empty($_SERVER['HTTPS']) || ($_SERVER['HTTPS'] == "off");
    $protocol = ($http) ? "http://" : "https://";
    header("Location: ".$protocol.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].$append);;
    exit();
  }

  private function sendNoCacheHeaders() {
    header("Expires: Tue, 27 Jul 1997 05:00:00 GMT");

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);

    header("Pragma: no-cache");
  }

  public function __construct() {
    global $DB_HOST;
    global $DB_USER;
    global $DB_PASSWORD;
    global $DB_NAME;

    session_start();
    if(isset($_SESSION["redirecting"])) {
      $this->m_messages = $_SESSION['messages'];
      unset($_SESSION['messages']);
      unset($_SESSION['redirecting']);
    }

    $this->sendNoCacheHeaders();

    $db = new db($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);

    //switching over the various actions possible
    if(!empty($_POST['create-folder'])) {
      $foldername = $_POST['foldername'];
      $noteid = $_POST['noteid'];
      if(empty($foldername)) {
        $this->m_messages[] = new Message("Folder name is empty, not doing anything",Message::SOMEERROR);
      } else {
        $this->m_messages[] = $db->createFolder($foldername);
      }
      $this->redirect($noteid);

    } elseif (!empty($_POST['create-note'])) {
      $folderId = $_POST['folder'];
      $notename = $_POST['notename'];
      $noteid = $_POST['noteid'];
      if($folderId<0) {
        $this->m_messages[] = new Message("No folder selected for note, doing nothing",Message::SOMEERROR);
      } elseif(empty($notename)) {
        $this->m_messages[] = new Message("Note name empty, doing nothing",Message::SOMEERROR);
      } else {
        $db->createNote($notename, $folderId);
      }
      $this->redirect($noteid);

    } elseif(!empty($_POST['update-note'])) {
      $data = $_POST['notetext'];
      $noteid = $_POST['noteid'];
      $db->updateNote($noteid, $data);
      $this->redirect($noteid);

    } elseif(!empty($_POST['delete-note'])) {
      $noteid = $_POST['noteid'];
      $iamsure = $_POST['iamsure'];
      if(empty($iamsure)) {
        $this->m_messages[] = new Message("\"I am sure\" not checked, not deleting note",Message::SOMEERROR);
      } else {
        $this->m_messages[] = $db->deleteNote($noteid);
        $noteid=-1;
      }
      $this->redirect($noteid);

    } elseif(!empty($_POST['delete-folder'])) {
      $folderid = $_POST['folderid'];
      $noteid = $_POST['noteid'];
      if ($folderid<0) {
        $this->m_messages[] = new Message("No folder selected, doing nothing",Message::SOMEERROR);
      } else {
        $this->m_messages[] = $db->deleteFolder($folderid);
      }
      $this->redirect($noteid);

    }

    $this->m_folders = $db->folders();

    if(isset($_GET["note"]) && is_numeric($_GET["note"])) {
      $this->m_current_note=$db->noteById($_GET["note"]);
    }
  }
}



?>
