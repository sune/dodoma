<?
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.


/**
 * class to represent a folder. A folder can contain 0 or more notes.
 */
class Folder {
  private $m_name;
  private $m_folderId;
  private $m_notes = array();

  public function __construct($name, $folderId, $notes ) {
    if(!is_array($notes)) {
      die('Folder got a non-array as $notes');
    }
    $this->m_notes=$notes;
    $this->m_folderId=$folderId;
    $this->m_name=$name;
  }

  public function name() {
    return $this->m_name;
  }

  /**
   * @return int with numeric id of the folder
   */

  public function id() {
    return $this->m_folderId;
  }

  public function notes() {
    return $this->m_notes;
  }

}

?>