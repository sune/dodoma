<?
//     Copyright (c) 2012 Sune Vuorela <sune@vuorela.dk>
//
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:
//
//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.


include("dodoma.php");

$dodoma = new Dodoma();

?>

<html>
  <head>
    <title><?echo $dodoma->title("Dodoma");?></title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
    <style>
      body {
        font-size: 11px;
        font-family: sans-serif;
      }
      #main {
        width: 100%;
      }
      #left {
        float: left;
        min-width: 180px;
        width: 15%;
        background: #eeeeee;
      }

      #maincontent {
        float: right;
        width: 85%;
      }

      #messages {
        clear:both;
      }

      .toolbox {
        border-top: 1px;
        border-left: 1px;
        border-bottom: 1px;
        border-right: 1px;
        border-style: dotted;
        border-color: #bbbbbb;
        margin: 5px;
        padding: 5px;
        background: #f5f5f5;
      }
      textarea {
        width: 100%;
        height: 85%;
      }
      textarea.modified {
        background-color: #ffdddd;
      }

      #logo {
        text-align: center;
      }

      #by {
        font-size: 8px;
        text-align:center;
      }

      a {
        text-decoration: none;
        color: #003BC3;
      }
      a:hover {
         text-decoration: underline;
      }
      select, input {
        border: 1px solid #003BC3;
        background: #ffffff;
	margin-top: 2px;
	margin-bottom: 2px;
      }
      .input {
	width: 100%;
      }
      .button {
        text-align: right;
      }
      select:hover, input:hover {
         border: 1px dotted #003BC3;
         background-color: #dddddd;
      }

    </style>
    <script>
      function swap_visibility(first_id,second_id) {
        var first = document.getElementById(first_id);
        var second = document.getElementById(second_id);
	var tmp = first.style.display;
	first.style.display=second.style.display;
	second.style.display=tmp;
      }
    </script>
  </head>
  <body onLoad="var txt = document.getElementById('maintext');txt.onkeyup=function() {document.getElementById('maintext').className = 'modified';  }; swap_visibility('toolbox','toolboxhidden');">
    <div id="main">
    <div id="left">
      <p id="logo">
        <a href="<?echo "";?>"><img src="images/logo.png" /></a>
      </p>
      <hr />
      <a onClick="swap_visibility('foldertree','foldertreehidden')"><h3>Notes <span id="foldertreehidden" style="display:none">&raquo;</span></h3></a>
      <?echo $dodoma->createFolderTree();?>
      <a onClick="swap_visibility('toolbox','toolboxhidden')"><h3>Toolbox <span id="toolboxhidden" style="display: none">&raquo;</span></h3></a>
      <?echo $dodoma->createToolBox();?>
    </div>
    <div id="maincontent">
      <?echo $dodoma->formatCurrentNote("<h1>Get Noting</h1>");?>
    </div>
    <?echo $dodoma->formatMessages();?>
    <div id="by">Powered by <a href="http://sune.vuorela.dk/dodoma">Dodoma</a> by <a href="http://sune.vuorela.dk">Sune Vuorela</a></div>
    </div>
  </body>
</html>
